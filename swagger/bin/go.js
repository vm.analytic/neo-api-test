#!/usr/bin/env node

let watch = require('watch')
    , mergeYaml = require('merge-yaml-cli')
    , fse = require('fs-extra')
    , fs = require('fs');

let path = '/var/www';
let outPath = path +'/build/swagger.yml';

function merge() {
    let result = mergeYaml.merge([
        path +'/src/docs.yaml',
        path +'/src/**/*.yaml',
        path +'/src/**/**/*.yaml',
        path +'/src/**/**/**/*.yaml',
        path +'/src/**/**/**/**/*.yaml'
    ]);
    fse.removeSync(outPath);

    fs.writeFile(outPath, result, function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
}


function watchCb() {
    console.log('Going to generate swagger.yml!');
    merge();
}

console.log('Welcome to swagger!');
console.log('Open http://localhost:8050/ to check documentation.');

watchCb();
watch.watchTree('/var/www/src', function (f, curr, prev) {
    if (typeof f == "object" && prev === null && curr === null) {
        // Finished walking the tree
    } else if (prev === null) {
        console.log("New file event");
        watchCb();
        // f is a new file
    } else if (curr.nlink === 0) {
        // f was removed
        console.log("File removed event");
        watchCb()
    } else {
        // f was changed
        console.log("File changed event");
        watchCb()
    }
});
