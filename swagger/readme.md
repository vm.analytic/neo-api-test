
# Personal News Swagger  
  
## Getting Started  

### Swagger links
1. [Specification](https://swagger.io/docs/specification/about/)
2. [Online swagger editor](https://editor.swagger.io/) 

### Requirements  

1. Docker & Docker Compose - needed if you want local version of swagger-ui & node.js watcher 

### Install  
  
1. Build docker-compose and run project 
    ```
    docker-compose up
    ``` 
2. Enjoy http://localhost:8050/

### Documentation

1. `make up` - alias to `docker-compose up -d`
2. `make down` - alias to `docker-compose down`