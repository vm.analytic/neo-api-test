IMAGE_TAG = latest
IMAGE_NAME = php

.PHONY: prepare
prepare:
	cp -n .env.dist .env

.PHONY: up
up:
	docker-compose up -d

build:
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker-compose build

.PHONY: down
down:
	docker-compose down

.PHONY: migrate
migrate:
	docker-compose exec $(IMAGE_NAME) php bin/console doctrine:migrations:migrate -n --allow-no-migration || true

require:
	docker-compose exec $(IMAGE_NAME) composer require

update:
	docker-compose exec $(IMAGE_NAME) composer update || true

enter:
	docker-compose exec $(IMAGE_NAME) bash

tail-logs:
	docker-compose logs -f $(IMAGE_NAME)

.PHONY: retrieve
retrieve:
	docker-compose exec $(IMAGE_NAME) php bin/console neo:nasa-sync || true

.PHONY: test
test:
	docker-compose exec $(IMAGE_NAME) php ./bin/phpunit || true

.PHONY: phpstan
stan:
	php -d memory_limit=1G vendor/bin/phpstan analyse -c phpstan.neon

.PHONY: swagger
swagger:
	(cd swagger/ && docker-compose up -d)

.PHONY: help
help: .title
	@echo ''
	@echo 'Usage: make [target] [ENV_VARIABLE=ENV_VALUE ...]'
	@echo ''
	@echo 'Available targets:'
	@echo ''
	@echo '  help          Show this help and exit'
	@echo '  up            Starts and attaches to containers for a service'
	@echo '  prepare       Prepare project.'
	@echo '  down          Stop, kill and purge project containers.'
	@echo '  update        Update composer dependencies.'
	@echo '  test          Run tests.'
	@echo '  stan          Run PHPStan analyzer.'
	@echo ''

%:
	@:

