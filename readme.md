NEO API
========

### Prepare

```bash
make prepare
```
### Start application
Insert your NASA_API_KEY in .env file. Then:
```bash
make build
make up
```

### Run migrations
```bash
make migrate
```
#### Now it is ready to use.
Do not forget add `Content-Type: application/json` to your request

### Get data from NASA API
```bash
make retrieve
```

### Run tests
```bash
make test
```

### Run swagger
Swagger UI will be available at port: `8050`
```bash
make swagger
```

### Rebuild application image

```bash
make build
```

### Uninstall

```bash
make down
```
