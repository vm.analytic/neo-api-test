<?php

namespace App\Services\Fractal;

use League\Fractal\TransformerAbstract;

abstract class JsonApiTransformer extends TransformerAbstract
{
    abstract public function getRoutingKey(): string;
}
