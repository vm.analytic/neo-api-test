<?php

namespace App\Services\Fractal;

use InvalidArgumentException;

final class Pagination
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * Pagination constructor.
     *
     * @param int $page
     * @param int $perPage
     */
    public function __construct(int $page = 1, int $perPage = 20)
    {
        if ($page <= 0 || $perPage <= 0) {
            throw new InvalidArgumentException('Wrong params received.');
        }
        $this->page = $page;
        $this->perPage = $perPage;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }
}
