<?php
declare(strict_types=1);

namespace App\Services\Fractal;

use Doctrine\ORM\Tools\Pagination\Paginator;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\TransformerAbstract;
use Symfony\Component\HttpFoundation\RequestStack;

class FractalService
{
    /**
     * @var FractalManager
     */
    private $fractalManager;

    public function __construct(DataArraySerializer $dataSerializer)
    {
        $this->fractalManager = (new FractalManager())->setSerializer($dataSerializer);
    }

    /**
     * @param mixed $item
     * @param TransformerAbstract $transformer
     *
     * @return array
     */
    public function item($item, TransformerAbstract $transformer): array
    {
        $resourceKey = $transformer instanceof JsonApiTransformer ? $transformer->getRoutingKey() : null;
        return $this->fractalManager
                    ->createData(new Item($item, $transformer, $resourceKey))
                    ->toArray();
    }

    /**
     * @param mixed $data
     * @param TransformerAbstract $transformer
     * @param Pagination|null $pagination
     * @return array
     */
    public function collection($data, TransformerAbstract $transformer, Pagination $pagination = null): array
    {
        $resourceKey = $transformer instanceof JsonApiTransformer ? $transformer->getRoutingKey() : null;
        $resource = new Collection($data, $transformer, $resourceKey);

        if ($data instanceof Paginator) {
            $resource->setPaginator(
                new DoctrinePaginatorAdapter($data, $pagination ?? new Pagination)
            );
        }

        return $this->fractalManager
                    ->createData($resource)
                    ->toArray();
    }
}
