<?php
declare(strict_types=1);

namespace App\Services\Fractal;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ResponseFinalizer
{
    /**
     * @var mixed
     */
    private $body;

    /**
     * ResponseFinalizer constructor.
     * @param mixed $body
     */
    public function __construct($body)
    {
        $this->body = $body;
    }

    /**
     * @param int $statusCode
     * @param array $headers
     * @return JsonResponse
     */
    public function asResponse(int $statusCode = Response::HTTP_OK, array $headers = []): JsonResponse
    {
        return new JsonResponse($this->body, $statusCode, $headers);
    }
}
