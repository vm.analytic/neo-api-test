<?php

namespace App\Services\Fractal;

use Doctrine\ORM\Tools\Pagination\Paginator;
use League\Fractal\Pagination\PaginatorInterface;

class DoctrinePaginatorAdapter implements PaginatorInterface
{
    private $paginator;
    private $pagination;

    /**
     * DoctrinePaginatorAdapter constructor.
     *
     * @param Paginator  $paginator
     * @param Pagination $pagination
     */
    public function __construct(Paginator $paginator, Pagination $pagination)
    {
        $this->paginator = $paginator;
        $this->pagination = $pagination;
    }

    public function getCurrentPage()
    {
        return $this->pagination->getPage();
    }

    public function getLastPage()
    {
        return ceil($this->getTotal() / $this->getPerPage());
    }

    public function getTotal()
    {
        return $this->paginator->count();
    }

    public function getCount()
    {
        return $this->paginator->getIterator()->count();
    }

    public function getPerPage()
    {
        return $this->pagination->getLimit();
    }

    public function getUrl($page)
    {
        return '/';
    }
}
