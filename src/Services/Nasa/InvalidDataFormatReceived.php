<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use Exception;

final class InvalidDataFormatReceived extends Exception
{
    protected $message = 'Obtained not familiar data format. Please check your input.';
}
