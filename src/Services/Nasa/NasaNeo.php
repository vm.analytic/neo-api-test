<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use DateTimeImmutable;

final class NasaNeo
{
    /**
     * @var int
     */
    private $nasaId;

    /**
     * @var int
     */
    private $referenceId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $speed;

    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var bool
     */
    private $isHazardous;

    /**
     * @var string
     */
    private $link;

    public function __construct(
        int $nasaId,
        int $referenceId,
        string $name,
        float $speed,
        bool $isHazardous,
        string $link,
        DateTimeImmutable $date
    ) {
        $this->nasaId = $nasaId;
        $this->referenceId = $referenceId;
        $this->name = $name;
        $this->speed = $speed;
        $this->date = $date;
        $this->isHazardous = $isHazardous;
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function nasaId(): int
    {
        return $this->nasaId;
    }

    /**
     * @return int
     */
    public function referenceId(): int
    {
        return $this->referenceId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function speed(): float
    {
        return $this->speed;
    }

    /**
     * @return DateTimeImmutable
     */
    public function date(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return bool
     */
    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }

    /**
     * @return string
     */
    public function link(): string
    {
        return $this->link;
    }
}
