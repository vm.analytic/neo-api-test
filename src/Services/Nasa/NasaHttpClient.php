<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use LogicException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class NasaHttpClient implements NasaAPIClient
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $url;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var Mapper
     */
    private $mapper;

    public function __construct(HttpClientInterface $httpClient, Mapper $mapper, string $baseUrl, string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->url = $baseUrl;
        $this->httpClient = $httpClient;
        $this->mapper = $mapper;
    }

    /**
     * The data is taken for the period with the inclusion of the last day!
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return iterable|NasaNeo[]
     * @throws NasaAPINotAvailable
     */
    public function all(DateTime $fromDate, DateTime $toDate): iterable
    {
        if ($toDate < $fromDate) {
            throw new LogicException();
        }

        try {
            $period = new DatePeriod(
                $fromDate,
                DateInterval::createFromDateString('1 day'),
                (clone $toDate)->modify('+1 day')
            );
            /** @var DateTime $date */
            foreach ($period as $date) {
                $response = json_decode($this->httpClient->request(
                    'GET',
                    $this->url,
                    [
                        'query' => [
                            'start_date' => $date->format('Y-m-d'),
                            'end_date' => $date->format('Y-m-d'),
                            'api_key' => $this->apiKey,
                        ],
                    ]
                )->getContent(), true);

                $NEOs = $response['near_earth_objects'][$date->format('Y-m-d')];
                foreach ($NEOs as $neo) {
                    yield $this->mapper->parse(DateTimeImmutable::createFromMutable($date), $neo);
                }
            }
        } catch (TransportExceptionInterface |
            RedirectionExceptionInterface |
            ClientExceptionInterface |
            ServerExceptionInterface $exception
        ) {
            throw new NasaAPINotAvailable;
        } catch (InvalidDataFormatReceived $exception) {
            throw new $exception;
        }
    }
}
