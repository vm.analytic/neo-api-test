<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use DateTime;

interface NasaAPIClient
{
    /**
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return iterable|NasaNeo[]
     */
    public function all(DateTime $fromDate, DateTime $toDate): iterable;
}
