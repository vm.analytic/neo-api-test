<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use Exception;

final class NasaAPINotAvailable extends Exception
{
    protected $message = 'NASA API not available at the moment.';
}
