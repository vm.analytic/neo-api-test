<?php
declare(strict_types=1);

namespace App\Services\Nasa;

use DateTimeImmutable;

final class Mapper
{
    /**
     * @param DateTimeImmutable $dateTimeImmutable
     * @param array $data
     * @return NasaNeo
     * @throws InvalidDataFormatReceived
     */
    public function parse(DateTimeImmutable $dateTimeImmutable, array $data): NasaNeo
    {
        try {
            return new NasaNeo(
                (int) $data['id'],
                (int) $data['neo_reference_id'],
                $data['name'],
                (float) $data['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'],
                $data['is_potentially_hazardous_asteroid'],
                strtok($data['links']['self'], '?'),
                $dateTimeImmutable
            );
        } catch (\Throwable $exception) {
            throw new InvalidDataFormatReceived;
        }
    }
}
