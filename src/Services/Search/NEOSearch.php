<?php
declare(strict_types=1);

namespace App\Services\Search;

use App\NEO\NEO;
use App\Services\Search\DTO\Month;
use App\Services\Search\DTO\Year;
use Doctrine\ORM\EntityManagerInterface;

final class NEOSearch
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function fastest(bool $isHazard): NEO
    {
        return $this->em->createQueryBuilder()
            ->from(NEO::class, 'n')
            ->select('n')
            ->where('n.isHazard = :is_hazard')
            ->setParameter('is_hazard', $isHazard)
            ->orderBy('n.speed', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function bestYear(bool $isHazard): Year
    {
        $queryResult = $this->em->createQueryBuilder()
            ->from(NEO::class, 'n')
            ->select('YEAR(n.date) as max_year', 'count(n) as HIDDEN cnt')
            ->where('n.isHazard = :is_hazard')
            ->setParameter('is_hazard', $isHazard)
            ->groupBy('max_year')
            ->orderBy('cnt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return new Year((int) $queryResult['max_year']);
    }

    public function bestMonth(bool $isHazard): Month
    {
        $queryResult = $this->em->createQueryBuilder()
            ->from(NEO::class, 'n')
            ->select('MONTH(n.date) as max_month', 'count(n) as HIDDEN cnt')
            ->where('n.isHazard = :is_hazard')
            ->setParameter('is_hazard', $isHazard)
            ->groupBy('max_month')
            ->orderBy('cnt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return new Month((int) $queryResult['max_month']);
    }
}
