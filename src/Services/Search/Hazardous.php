<?php

namespace App\Services\Search;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

class Hazardous extends BaseSpecification
{
    /**
     * @var bool
     */
    private $isHazard;

    public function __construct(bool $isHazard, $dqlAlias = null)
    {
        $this->isHazard = $isHazard;
        parent::__construct($dqlAlias);
    }

    protected function getSpec()
    {
        return Spec::andX(
            Spec::eq('isHazard', $this->isHazard)
        );
    }
}
