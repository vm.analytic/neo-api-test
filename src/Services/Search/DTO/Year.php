<?php
declare(strict_types=1);

namespace App\Services\Search\DTO;

final class Year
{

    /**
     * @var int
     */
    private $year;

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function value(): int
    {
        return $this->year;
    }
}
