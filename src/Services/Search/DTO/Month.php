<?php
declare(strict_types=1);

namespace App\Services\Search\DTO;

use DateTime;

final class Month
{

    /**
     * @var int
     */
    private $monthNumber;

    public function __construct(int $monthNumber)
    {
        $this->monthNumber = $monthNumber;
    }

    public function value(): string
    {
        return DateTime::createFromFormat('!m', (string) $this->monthNumber)->format('F');
    }
}
