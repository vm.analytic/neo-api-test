<?php
declare(strict_types=1);

namespace App\Services\Search;

use App\NEO\NEO;
use App\NEO\NEORepository;
use App\Services\Fractal\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Happyr\DoctrineSpecification\EntitySpecificationRepository;
use Happyr\DoctrineSpecification\Specification\Specification;

final class DoctrineNEORepository implements NEORepository
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntitySpecificationRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(NEO::class);
    }

    /**
     * @inheritDoc
     */
    public function add(NEO $NEO): void
    {
        $this->em->persist($NEO);
    }

    /**
     * @inheritDoc
     */
    public function list(Specification $specification, Pagination $pagination = null)
    {
        $query = $this->repo->getQuery($specification);

        if ($pagination) {
            $query = $this->applyPagination(
                $query,
                $pagination
            );
        }

        return $pagination ? new Paginator($query) : $query->execute();
    }

    private function applyPagination(Query $query, Pagination $pagination): Query
    {
        $query->setMaxResults($pagination->getLimit());
        $query->setFirstResult($pagination->getOffset());

        return $query;
    }
}
