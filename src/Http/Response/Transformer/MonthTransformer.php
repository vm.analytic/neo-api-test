<?php

namespace App\Http\Response\Transformer;

use App\NEO\NEO;
use App\Services\Fractal\JsonApiTransformer;
use App\Services\Search\DTO\Month;
use League\Fractal\TransformerAbstract;

class MonthTransformer extends TransformerAbstract
{
    public function transform(Month $month): array
    {
        return [
            'month' => $month->value()
        ];
    }
}
