<?php

namespace App\Http\Response\Transformer;

use App\NEO\NEO;
use League\Fractal\TransformerAbstract;

class NEOTransformer extends TransformerAbstract
{
    public function transform(NEO $neo): array
    {
        return [
            'id'           => $neo->id(),
            'reference'    => $neo->reference(),
            'name'         => $neo->name(),
            'speed'        => $neo->speed(),
            'is_hazardous' => $neo->isHazard(),
            'date'         => $neo->date()->format('Y-m-d'),
        ];
    }
}
