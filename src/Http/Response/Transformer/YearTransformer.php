<?php

namespace App\Http\Response\Transformer;

use App\Services\Search\DTO\Year;
use League\Fractal\TransformerAbstract;

class YearTransformer extends TransformerAbstract
{
    public function transform(Year $year): array
    {
        return [
            'year' => $year->value()
        ];
    }
}
