<?php
declare(strict_types=1);

namespace App\Http\Controller;

use App\Http\Request\NEO\HazardousSearchRequest;
use App\Http\Response\Transformer\MonthTransformer;
use App\Http\Response\Transformer\NEOTransformer;
use App\Http\Response\Transformer\YearTransformer;
use App\NEO\NEORepository;
use App\Services\Fractal\Pagination;
use App\Services\Search\Hazardous;
use App\Services\Search\NEOSearch;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/neo")
 */
class NEOController extends ApiController
{
    /**
     * @Route("/hazardous", methods={"GET"})
     * @param NEOTransformer $transformer
     * @param NEORepository $repository
     * @param Pagination $pagination
     * @return JsonResponse
     */
    public function hazardous(NEOTransformer $transformer, NEORepository $repository, Pagination $pagination)
    {
        return $this->collection(
            $repository->list(new Hazardous(true), $pagination),
            $transformer,
            $pagination
        )->asResponse();
    }

    /**
     * @Route("/fastest", methods={"GET"})
     * @param HazardousSearchRequest $request
     * @param NEOTransformer $transformer
     * @param NEOSearch $search
     * @return JsonResponse
     */
    public function fastest(HazardousSearchRequest $request, NEOTransformer $transformer, NEOSearch $search)
    {
        return $this->item(
            $search->fastest($request->hazardous),
            $transformer
        )->asResponse();
    }

    /**
     * @Route("/best-year", methods={"GET"})
     * @param HazardousSearchRequest $request
     * @param YearTransformer $transformer
     * @param NEOSearch $search
     * @return JsonResponse
     */
    public function bestYear(HazardousSearchRequest $request, YearTransformer $transformer, NEOSearch $search)
    {
        return $this->item(
            $search->bestYear($request->hazardous),
            $transformer
        )->asResponse();
    }

    /**
     * @Route("/best-month", methods={"GET"})
     * @param HazardousSearchRequest $request
     * @param MonthTransformer $transformer
     * @param NEOSearch $search
     * @return JsonResponse
     */
    public function bestMonth(HazardousSearchRequest $request, MonthTransformer $transformer, NEOSearch $search)
    {
        return $this->item(
            $search->bestMonth($request->hazardous),
            $transformer
        )->asResponse();
    }
}
