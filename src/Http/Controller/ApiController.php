<?php
declare(strict_types=1);

namespace App\Http\Controller;

use App\Services\Fractal\FractalService;
use App\Services\Fractal\Pagination;
use App\Services\Fractal\ResponseFinalizer;
use League\Fractal\TransformerAbstract;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class ApiController extends AbstractController
{
    /**
     * @var FractalService
     */
    private $service;

    public function __construct(FractalService $service)
    {
        $this->service = $service;
    }

    protected function item($data, TransformerAbstract $transformer, array $additionalMeta = []): ResponseFinalizer
    {
        return new ResponseFinalizer(
            array_merge($this->service->item($data, $transformer), $additionalMeta)
        );
    }

    protected function collection(
        $data,
        TransformerAbstract $transformer,
        Pagination $pagination = null,
        array $additionalMeta = []
    ): ResponseFinalizer {
        $data = $this->service->collection($data, $transformer, $pagination);
        if (isset($data['meta'])) {
            $data['meta'] = array_merge($data['meta'], $additionalMeta);
        }

        return new ResponseFinalizer($data);
    }
}
