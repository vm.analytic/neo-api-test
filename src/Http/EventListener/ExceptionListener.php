<?php
declare(strict_types=1);

namespace App\Http\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    /**
     * @var string
     */
    private $env;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $route;

    public function __construct(
        string $env,
        LoggerInterface $logger,
        RequestStack $requestStack
    ) {
        $this->env = $env;
        $this->logger = $logger;
        $this->route = $requestStack->getMasterRequest()->attributes->get('_route');
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $response = new JsonResponse();

        //TODO: Add logic here
        $data = [
            'code' => $response->getStatusCode(),
            'message' => $event->getException()->getMessage(),
        ];

        $response->setData($data);
        $event->setResponse($response);
    }
}
