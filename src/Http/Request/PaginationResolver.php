<?php

namespace App\Http\Request;

use App\Services\Fractal\Pagination;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class PaginationResolver implements ArgumentValueResolverInterface
{
    /**
     * @inheritDoc
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getType() === Pagination::class;
    }

    /**
     * @inheritDoc
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        yield new Pagination(
            $request->get('page', 1),
            $request->get('per_page', 20)
        );
    }
}
