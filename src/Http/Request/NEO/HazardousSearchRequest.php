<?php
declare(strict_types=1);

namespace App\Http\Request\NEO;

use App\Http\Request\RequestData;
use Symfony\Component\Validator\Constraints as Assert;

final class HazardousSearchRequest implements RequestData
{
    /**
     * @Assert\Type("bool")
     */
    public $hazardous;

    public function __construct()
    {
        $this->hazardous = false;
    }

    public function setHazardous($isHazardous)
    {
        $this->hazardous = filter_var($isHazardous, FILTER_VALIDATE_BOOLEAN);
    }
}
