<?php
declare(strict_types=1);

namespace App\Http\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JsonRequestDataResolver implements ArgumentValueResolverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * RequestDataResolver constructor.
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     */
    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }


    /**
     * Whether this resolver can resolve the value for the given ArgumentMetadata.
     *
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $request->getContentType() === 'json' &&
            is_subclass_of($argument->getType(), RequestData::class);
    }

    /**
     * Returns the possible value(s).
     *
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return \Generator
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        if (Request::METHOD_GET === $request->getMethod()) {
            $data = json_encode($request->query->all());
        } else {
            $data = $request->getContent();
        }

        /** @var RequestData $data */
        $data = $this->serializer->deserialize($data, $argument->getType(), 'json');
        $this->validate($data);

        yield $data;
    }

    /**
     * @param RequestData $data
     */
    private function validate(RequestData $data)
    {
        $errors = $this->validator->validate($data);

        if (0 !== count($errors)) {
            $json = $this->serializer->serialize($errors, 'json');
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, $json);
        }
    }
}
