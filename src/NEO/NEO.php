<?php
declare(strict_types=1);

namespace App\NEO;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(readOnly=true)
 */
class NEO
{
    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", unique=true)
     */
    private $nasaId;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $reference;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $speed;

    /**
     * @var DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isHazard;

    public function __construct(
        int $nasaId,
        int $reference,
        string $name,
        float $speed,
        bool $isHazard,
        DateTimeImmutable $date
    ) {
        $this->id = Uuid::uuid4();
        $this->nasaId = $nasaId;
        $this->name = $name;
        $this->speed = $speed;
        $this->isHazard = $isHazard;
        $this->reference = $reference;
        $this->date = $date;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function nasaId(): int
    {
        return $this->nasaId;
    }

    /**
     * @return int
     */
    public function reference(): int
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function speed(): float
    {
        return $this->speed;
    }

    /**
     * @return DateTimeImmutable
     */
    public function date(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return bool
     */
    public function isHazard(): bool
    {
        return $this->isHazard;
    }
}
