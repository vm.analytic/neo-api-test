<?php
declare(strict_types=1);

namespace App\NEO;

use App\Services\Fractal\Pagination;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Happyr\DoctrineSpecification\Specification\Specification;

interface NEORepository
{
    /**
     * @param NEO $NEO
     */
    public function add(NEO $NEO): void;

    /**
     * @param Specification $specification
     * @param Pagination|null $pagination
     * @return Paginator|NEO[]
     */
    public function list(Specification $specification, Pagination $pagination = null);
}
