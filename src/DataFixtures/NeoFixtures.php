<?php

namespace App\DataFixtures;

use App\NEO\NEO;
use DateTimeImmutable;
use Doctrine\Common\Persistence\ObjectManager;

class NeoFixtures extends AppFixtures
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::FIXTURES_SIZE; $i++) {
            $neo = new NEO(
                $this->faker->randomDigitNotNull,
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(),
                $this->faker->randomElement([true, false]),
                DateTimeImmutable::createFromMutable($this->faker->dateTime)
            );
            $manager->persist($neo);
        }
        $manager->flush();
    }
}
