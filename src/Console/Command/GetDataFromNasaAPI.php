<?php

namespace App\Console\Command;

use App\NEO\NEO;
use App\NEO\NEORepository;
use App\Services\Nasa\NasaAPIClient;
use DateInterval;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetDataFromNasaAPI extends Command
{

    /**
     * @var NasaAPIClient
     */
    private $client;

    /**
     * @var NEORepository
     */
    private $repository;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    public function __construct(
        NasaAPIClient $client,
        NEORepository $repository,
        ManagerRegistry $managerRegistry,
        string $name = null
    ) {
        parent::__construct($name);
        $this->client = $client;
        $this->repository = $repository;
        $this->managerRegistry = $managerRegistry;
    }

    protected function configure()
    {
        $this->setName('neo:nasa-sync')
            ->setDescription('Get data from NASA API')
            ->addArgument(
                'days',
                InputArgument::OPTIONAL,
                'How many days you want to retrieve?',
                3
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $days = $input->getArgument('days');
        $to = new \DateTime();
        /** @noinspection PhpUnhandledExceptionInspection */
        $from = (clone $to)->sub(new DateInterval('P' . $days .'D'));

        foreach ($this->client->all($from, $to) as $item) {
            try {
                $neo = new NEO(
                    $item->nasaId(),
                    $item->referenceId(),
                    $item->name(),
                    $item->speed(),
                    $item->isHazardous(),
                    $item->date()
                );
                $this->repository->add($neo);
                $this->managerRegistry->getManager()->flush();
            } catch (UniqueConstraintViolationException $exception) {
                $output->writeln('<error>Duplication! NEO with such nasa_id already exist!</error>');
                $this->managerRegistry->resetManager();
            }
        }
    }
}
