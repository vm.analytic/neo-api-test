<?php
declare(strict_types = 1);

namespace App\Tests\PHPUnit\Functional;

use App\NEO\NEO;
use DateTimeImmutable;

class HazardousActionTest extends FunctionalTestCase
{
    /**
     * @var string
     */
    private $url = '/neo/hazardous';

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures();
    }

    public function testResponseCodeOk()
    {
        $this->client->request('GET', $this->url);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty(json_decode($response->getContent(), true)['data']);

        return json_decode($response->getContent(), true)['data'];
    }

    /**
     * @depends testResponseCodeOk
     * @param array $responseContent
     */
    public function testAllKeysPresent(array $responseContent)
    {
        foreach ($responseContent as $neo) {
            $this->assertArrayHasKey('id', $neo);
            $this->assertArrayHasKey('reference', $neo);
            $this->assertArrayHasKey('name', $neo);
            $this->assertArrayHasKey('speed', $neo);
            $this->assertArrayHasKey('is_hazardous', $neo);
            $this->assertArrayHasKey('date', $neo);
        }
    }

    /**
     * @depends testResponseCodeOk
     * @param array $responseContent
     */
    public function testOnlyHazardousPresentInResponse(array $responseContent)
    {
        $this->assertCount(10, $responseContent);
        foreach ($responseContent as $neo) {
            $this->assertTrue($neo['is_hazardous']);
        }
    }

    private function loadFixtures(): void
    {
        $this->entityManager->createQuery('DELETE FROM ' . NEO::class)->execute();

        for ($i = 0; $i < 10; $i++) {
            $neo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(),
                true,
                DateTimeImmutable::createFromMutable($this->faker->dateTime)
            );

            $this->entityManager->persist($neo);
        }

        for ($i = 0; $i < 5; $i++) {
            $neo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(),
                false,
                DateTimeImmutable::createFromMutable($this->faker->dateTime)
            );

            $this->entityManager->persist($neo);
        }
        $this->entityManager->flush();
    }
}
