<?php
declare(strict_types = 1);

namespace App\Tests\PHPUnit\Functional;

use App\NEO\NEO;
use DateTimeImmutable;

class FastestActionTest extends FunctionalTestCase
{
    /**
     * @var string
     */
    private $url = '/neo/fastest';

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures();
    }

    public function testResponseCodeOk()
    {
        $this->client->request('GET', $this->url);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty(json_decode($response->getContent(), true)['data']);

        return json_decode($response->getContent(), true)['data'];
    }

    /**
     * @depends testResponseCodeOk
     * @param array $responseContent
     */
    public function testMaxSpeed(array $responseContent)
    {
        $this->assertEquals(2000, $responseContent['speed']);
    }

    private function loadFixtures(): void
    {
        $this->entityManager->createQuery('DELETE FROM ' . NEO::class)->execute();

        for ($i = 0; $i < 10; $i++) {
            $neo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(3, 10, 1000),
                true,
                DateTimeImmutable::createFromMutable($this->faker->dateTime)
            );

            $this->entityManager->persist($neo);
        }

        for ($i = 0; $i < 5; $i++) {
            $neo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(3, 10, 1000),
                false,
                DateTimeImmutable::createFromMutable($this->faker->dateTime)
            );

            $this->entityManager->persist($neo);
        }

        $neoNotHazard = new NEO(
            $this->faker->unique(true)->randomNumber(5, true),
            $this->faker->randomDigitNotNull,
            $this->faker->word,
            2000,
            false,
            DateTimeImmutable::createFromMutable($this->faker->dateTime)
        );

        $neoHazard = new NEO(
            $this->faker->unique(true)->randomNumber(5, true),
            $this->faker->randomDigitNotNull,
            $this->faker->word,
            3000.321,
            true,
            DateTimeImmutable::createFromMutable($this->faker->dateTime)
        );
        $this->entityManager->persist($neoNotHazard);
        $this->entityManager->persist($neoHazard);
        $this->entityManager->flush();
    }
}
