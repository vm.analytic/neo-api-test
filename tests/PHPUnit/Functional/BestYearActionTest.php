<?php
declare(strict_types = 1);

namespace App\Tests\PHPUnit\Functional;

use App\NEO\NEO;
use DateTimeImmutable;

class BestYearActionTest extends FunctionalTestCase
{
    /**
     * @var string
     */
    private $url = '/neo/best-year';

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures();
    }

    public function testResponseCodeOk()
    {
        $this->client->request('GET', $this->url);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty(json_decode($response->getContent(), true)['data']);

        return json_decode($response->getContent(), true)['data'];
    }

    /**
     * @depends testResponseCodeOk
     * @param array $responseContent
     */
    public function testMaxSpeed(array $responseContent)
    {
        $this->assertEquals(2019, $responseContent['year']);
    }

    private function loadFixtures(): void
    {
        $this->entityManager->createQuery('DELETE FROM ' . NEO::class)->execute();

        for ($i = 0; $i < 10; $i++) {
            $hazardNeo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(3, 10, 1000),
                true,
                new DateTimeImmutable()
            );

            $notHazardNeo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(3, 10, 1000),
                false,
                new DateTimeImmutable()
            );

            $this->entityManager->persist($hazardNeo);
            $this->entityManager->persist($notHazardNeo);
        }

        for ($i = 0; $i < 5; $i++) {
            $neo = new NEO(
                $this->faker->unique(true)->randomNumber(5, true),
                $this->faker->randomDigitNotNull,
                $this->faker->word,
                $this->faker->randomFloat(3, 10, 1000),
                false,
                new DateTimeImmutable('2016-01-20')
            );

            $this->entityManager->persist($neo);
        }
        $this->entityManager->flush();
    }
}
