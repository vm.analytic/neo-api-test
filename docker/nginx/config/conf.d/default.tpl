server {
    location ~ ^/(_profiler|neo)/.* {
        fastcgi_pass php:9000;

        fastcgi_param  SCRIPT_FILENAME    /app/public/index.php;
        fastcgi_param  SCRIPT_NAME        index.php;

        fastcgi_buffer_size 128k;
        fastcgi_buffers 256 16k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;

        include fastcgi_params;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/oauth_error.log;
    access_log /var/log/nginx/oauth_access.log;
}
